package mychatappp.data;

import mychatappp.MyChatAppp;
import mychatappp.Server;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class Client {

    public Socket socket;
    private Thread listening;
    private Thread sending;
    private String ip;
    private String name;
    private int port;
    private boolean running = true;
    private boolean isChanged = false;

    public Client(Socket socket, int port, String ip, String name){
        this.socket = socket;
        this.port = port;
        this.name = name;
        this.ip = ip;
    }
    public void startListening() {
       listening = new Thread(new Runnable() {
            public void run() {
                while (running) {
                    try {
                        do {
                            BufferedReader inFromClient =
                                    new BufferedReader(new InputStreamReader(socket.getInputStream()));
                            String message = inFromClient.readLine();
//                            System.out.println(ip+" "+name+":\t\t\t"+message);
                            if (message!=null) {
                                performAction(message);
                            }
                        } while (socket.getInputStream().available() > 0);
                    } catch (IOException e) {
                        running = false;
                        e.printStackTrace();
                    }
                }
            }
        });
       listening.start();
    }

    public void stopListening(){
        running = false;
        listening.interrupt();
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void performAction(String action){
        JSONObject obj = new JSONObject(action);
        if (obj.get("action").equals("subscribe")){
            subscribeAction(obj);
        }
        if (obj.get("action").equals("unsubscribe")){
            unsubscribeAction(obj);
        }
    }

    private void subscribeAction(JSONObject obj){
        JSONArray groups = Server.getGroups();
        JSONObject data = new JSONObject();
        data.put("name",name);
        data.put("ip",ip);
        for(int i=0;i<groups.length();i++) {
            if (groups.getJSONObject(i).getInt("port")==obj.getInt("port")) {
                groups.getJSONObject(i).getJSONArray("users").put(data);
            }
        }
        Server.repository.rewriteJsonByLine(MyChatAppp.currentDev+"/groups.txt",groups);
        setChanged();
    }

    private void unsubscribeAction(JSONObject obj){
        JSONArray groups = Server.getGroups();
        for(int i=0;i<groups.length();i++) {
            if (groups.getJSONObject(i).getInt("port")==obj.getInt("port")) {
                for (int j=0;j<groups.getJSONObject(i).getJSONArray("users").length();j++){
                    if (groups.getJSONObject(i).getJSONArray("users").getJSONObject(j).get("name").equals(name)
                            && groups.getJSONObject(i).getJSONArray("users").getJSONObject(j).get("ip").equals(ip)) {
                        groups.getJSONObject(i).getJSONArray("users").remove(j);
                    }
                }
            }
        }
        Server.repository.rewriteJsonByLine(MyChatAppp.currentDev+"/groups.txt",groups);
        setChanged();
    }

    public JSONArray getMyGroups(){
        JSONArray allGroups = Server.getGroups();
        JSONArray result = new JSONArray();
        for (int i=0;i<allGroups.length();i++){
            boolean atGroup = false;
            for (int j=0;j<allGroups.getJSONObject(i).getJSONArray("users").length();j++){
                if (ip.equals(allGroups.getJSONObject(i).getJSONArray("users").getJSONObject(j).get("ip"))
                        && name.equals(allGroups.getJSONObject(i).getJSONArray("users").getJSONObject(j).get("name"))
                        )
                    atGroup = true;
            }
            JSONObject group = new JSONObject();
            group.put("name",allGroups.getJSONObject(i).get("name"));
            group.put("port",allGroups.getJSONObject(i).getInt("port"));
            if (atGroup)
                group.put("here",1);
            else group.put("here",0);
            result.put(group);
        }
        setUnchagned();
        return result;
    }

    public void Echo(String message) throws IOException {
        DataOutputStream stream = new DataOutputStream(socket.getOutputStream());
        stream.writeBytes(message+"\n");
    }

    public boolean isEqual(String str) {
        String compareString = this.name+" "+this.ip+":"+this.port;
        return compareString.equals(str);
    }

    public boolean isChanged(){
        return  isChanged;
    }

    private void setChanged(){
        System.out.println(name+" is changed");
        isChanged = true;
    }

    private void setUnchagned(){
        System.out.println(name+" is unchanged");
        isChanged = false;
    }

    public boolean isActive(){
        try {
            Echo("Test");
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    public String getName() {
        return name;
    }
}
