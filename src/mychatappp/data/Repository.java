package mychatappp.data;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.util.ArrayList;

public class Repository {
    public JSONArray readFileToJson(String path) {
        ArrayList<String> list = this.readFileToStringArray(path);
        return new JSONArray("["+String.join(",",list)+"]");
    }

    public ArrayList<String> readFileToStringArray(String path) {
        BufferedReader buffer = null;
        FileReader reader = null;
        ArrayList<String> strings = new ArrayList<String>();

        try {
            reader = new FileReader(path);
            buffer = new BufferedReader(reader);
            String currentLine;
            while ((currentLine = buffer.readLine()) != null) {
                strings.add(currentLine);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return strings;
    }

    public boolean rewriteJsonToFile(String path, JSONArray array) {
        String string = array.toString();
        return this.rewriteStringToFile(path, string.substring(1,string.length()-2), true);
    }

    public boolean rewriteJsonByLine(String path, JSONArray array){
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(path, false), "UTF-8"));
            writer.write("");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (int i=0;i<array.length();i++){
            addJsonToFile(path,array.getJSONObject(i));
        }
        return true;
    }

    public boolean addJsonToFile(String path, JSONObject line) {
        return this.addStringToFile(path,line.toString(), true);
    }

    public boolean rewriteStringToFile(String path, String text, boolean newLine) {
            BufferedWriter writer = null;
            try {
                writer = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream(path, false), "UTF-8"));
                writer.write(text);
                if (newLine) writer.newLine();
                writer.flush();
                writer.close();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return true;
    }

    public boolean addStringToFile(String path, String text, boolean newLine) {
        if (issetFile(path)){
            try {
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream(path, true), "UTF-8"));
                writer.append(text);
                if (newLine) writer.newLine();
                writer.flush();
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        }
        else {
            try {
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream(path, false), "UTF-8"));
                writer.write(text);
                if (newLine) writer.newLine();
                writer.flush();
                writer.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public boolean issetDirectory(String path) {
        File Directory = new File(path);
        return Directory.exists();
    }

    public boolean isDirectory(String path) {
        File Directory = new File(path);
        return Directory.isDirectory();
    }

    public boolean issetFile(String path) {
        File file = new File(path);
        return file.exists();
    }

    public boolean isFile(String path) {
        File file = new File(path);
        return file.isFile();
    }
}
