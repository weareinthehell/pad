package mychatappp;

import org.json.JSONArray;
import org.json.JSONObject;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;

public class Router {

    private static Socket socket;
    public static JSONArray groups;

    public static void main(String[] args) throws IOException {
        RouteFrame frame = new RouteFrame();
    }

    public static void setSocket(Socket socket) {
        Router.socket = socket;
    }

    public static Socket getSocket() {
        return socket;
    }

    static class RouteFrame extends JFrame{
        Container c;
        RouteFrame() throws IOException {
            super("Router");
            setDefaultCloseOperation(EXIT_ON_CLOSE);
            setSize(new Dimension(300, 150));
            setResizable(false);
            c = getContentPane();
            setUI();
            setVisible(true);
            setSocket(new Socket(MyChatAppp.host, 8999));
            DataOutputStream outToServer = new DataOutputStream(getSocket().getOutputStream());
            outToServer.writeBytes(MyChatAppp.getIp()+" Router\n");
            BufferedReader inFromClient =
                    new BufferedReader(new InputStreamReader(getSocket().getInputStream()));

            String message = inFromClient.readLine();

            JSONObject obj = new JSONObject(message);
            groups = obj.getJSONArray("groups");
        }

        private void setUI(){
            c.setLayout(new BorderLayout());
            JTextField message = new JTextField();
            c.add(message,BorderLayout.NORTH);

            JButton sendButton = new JButton();
            sendButton.setText("Send");
            sendButton.setMaximumSize(new Dimension(300,50));
            c.add(sendButton,BorderLayout.SOUTH);

            sendButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    if (message.getText().length()>0){
                        try {
                            send(message.getText());
                            message.setText("");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }

        private void send(String text) throws IOException {
            ArrayList<Integer> sendTo = new ArrayList<>();

            if (text.substring(0,1).toLowerCase().equals("a") || text.substring(0,1).toLowerCase().equals("b")){
                sendTo.add(groups.getJSONObject(0).getInt("port"));
            }

            if (text.substring(0,1).toLowerCase().equals("c") || text.substring(0,1).toLowerCase().equals("d")){
                sendTo.add(groups.getJSONObject(1).getInt("port"));
            }

            if (text.substring(0,1).toLowerCase().equals("x") || text.substring(0,1).toLowerCase().equals("y")){
                sendTo.add(groups.getJSONObject(2).getInt("port"));
            }

            if (sendTo.size()==0){
                for (int i=0;i<groups.length();i++)
                    sendTo.add(groups.getJSONObject(i).getInt("port"));
            }

            GregorianCalendar c = new GregorianCalendar();
            SimpleDateFormat formatedDate = new SimpleDateFormat("dd.MM.YYYY k:m:s");

            JSONObject ms = new JSONObject();
            ms.put("port","");
            ms.put("name","Router");
            ms.put("message",text);
            ms.put("date",formatedDate.format(c.getTime()));

            for(int i=0;i<sendTo.size();i++){
                Socket sendSocket = new Socket(MyChatAppp.host,sendTo.get(i));
                DataOutputStream stream = new DataOutputStream(sendSocket.getOutputStream());
                stream.writeBytes(ms.toString()+"\n");
//                sendSocket.close();
            }
        }

    }
}
