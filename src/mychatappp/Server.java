package mychatappp;

import mychatappp.data.Client;
import mychatappp.data.Repository;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

public class Server {
    private static ServerSocket socket;
    public static Repository repository;
    private static int start_port = 9000;
    public static ArrayList<Client> clients = new ArrayList<>();
    public static ArrayList<ServerSocket> groups = new ArrayList<>();
    private static HashMap<Integer,ArrayList<Socket>> groupsClients = new HashMap<>();

    static {
        repository = new Repository();
    }

    private static void startGroups() throws IOException {
        JSONArray groups_ = getGroups();

        for(int i=0;i<groups_.length();i++){
            System.out.println("Server for "+groups_.getJSONObject(i).get("name")+" started");
            groups.add(new ServerSocket(groups_.getJSONObject(i).getInt("port")));
            int finalI = i;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        listeningGroup(groups.get(finalI),groups_.getJSONObject(finalI));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }

    private static void listeningGroup(ServerSocket server, JSONObject group) throws IOException {
        while(true) {
            Socket connectionSocket = server.accept();
            if(!groupsClients.containsKey(server.getLocalPort())){
                groupsClients.put(server.getLocalPort(),new ArrayList<Socket>());
            }
            groupsClients.get(server.getLocalPort()).add(connectionSocket);

            System.out.println("Connected to group "+group.get("name"));
            sendMessagesToUser(System.getProperty("user.dir")+"/"+group.getString("name")+".txt",connectionSocket);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    while (true) {
                        try {
                            do {
                                BufferedReader inFromClient =
                                        new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
                                String message = inFromClient.readLine();
                                if (message!=null) {
                                    JSONObject ms = new JSONObject(message);
                                    echoToOthers(ms,connectionSocket.getLocalPort());
                                    repository.addJsonToFile(System.getProperty("user.dir")+"/"+group.getString("name")+".txt",ms);
                                }
                            } while (connectionSocket.getInputStream().available() > 0);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }).start();
        }
    }
    private static void sendMessagesToUser(String file, Socket socket) throws IOException {
        JSONArray messages = repository.readFileToJson(file);
        JSONObject object = new JSONObject();
        object.put("messages",messages);
        object.put("action","store");
        DataOutputStream stream = new DataOutputStream(socket.getOutputStream());
        stream.writeBytes(object.toString() + "\n");
    }

    private static void echoToOthers(JSONObject message, int group){
        ArrayList<Integer> ids = new ArrayList<>();
        for (int i=0;i<groupsClients.get(group).size();i++){
            try {
                DataOutputStream stream = new DataOutputStream(groupsClients.get(group).get(i).getOutputStream());
                stream.writeBytes(message.toString() + "\n");
            } catch (IOException e) {
                e.printStackTrace();
                ids.add(i);
            }
        }
        for (int i=0;i<ids.size();i++)
            groupsClients.get(group).remove(ids.get(i)-i);

    }

    public static void main(String[] args) throws IOException {
        socket = new ServerSocket(8999);
        System.out.println("Server started");
        startGroups();
        while(true){
            Socket connectionSocket = socket.accept();
                BufferedReader inFromClient =
                        new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));

                String message = inFromClient.readLine();
                String[] credentials = getCredentials(message);
                int userPort = addClientToList(message);
                Client client = new Client(connectionSocket,userPort,credentials[0],credentials[1]);
                clients.add(client);
                client.startListening();

                JSONObject returnToClientData = new JSONObject();
                returnToClientData.put("port",userPort);
                returnToClientData.put("groups",client.getMyGroups());
                returnToClientData.put("users",users());

                DataOutputStream stream = new DataOutputStream(connectionSocket.getOutputStream());
                stream.writeBytes(returnToClientData.toString()+"\n");
                checkOnlineUsers();
        }
    }

    private static JSONArray users(){
        JSONArray array = new JSONArray();
        for (int i=0;i<clients.size();i++){
            array.put(clients.get(i).getName());
        }
        return array;
    }

    private static void checkOnlineUsers(){
        int current = clients.size();
        System.out.println("GLOBAL Connected: " + current);
        Timer timer = new Timer();
        TimerTask myTask = new TimerTask() {
            @Override
            public void run() {
                JSONArray array = new JSONArray();
                int current = clients.size();
                for (int i=0;i<clients.size();i++){
                    array.put(clients.get(i).getName());
                }

                for (int i=0;i<clients.size();i++){
                    JSONObject response = new JSONObject();
                    response.put("users",array);
                    if (clients.get(i).isChanged()) {
                         response.put("groups", clients.get(i).getMyGroups());
                    }
                    try {
                        clients.get(i).Echo(response.toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                        clients.get(i).stopListening();
                        clients.remove(i);
                    }
                }
                if (clients.size()!=current) {
                    current = clients.size();
                    System.out.println("GLOBAL Connected: " + clients.size());
                }
            }
        };

        timer.schedule(myTask, 1000, 1000);
    }

    private static String[] getCredentials(String message){
        return message.split(" ");
    }

    private static int addClientToList(String message){
        String[] credentials = getCredentials(message);
        String currentPath = System.getProperty("user.dir");
        if (repository.issetFile(currentPath+"/credentials.txt")) {
            JSONArray ipList = repository.readFileToJson(currentPath+"/credentials.txt");
            for (int i=0;i<ipList.length();i++){
                if (ipList.getJSONObject(i).getString("ip").equals(credentials[0])
                        &&ipList.getJSONObject(i).getString("name").equals(credentials[1])){
                    return ipList.getJSONObject(i).getInt("port");
                }
            }

            return writeToFile(credentials[1],credentials[0],start_port++);
        }
        else{
           return writeToFile(credentials[1],credentials[0],start_port++);
        }
    }

    private static  int writeToFile(String name, String ip, int port){
        String currentPath = System.getProperty("user.dir");
        JSONObject object = new JSONObject();
        object.put("name",name);
        object.put("ip", ip);
        object.put("port",port);

        repository.addJsonToFile(currentPath+"/credentials.txt",object);
        return port;
    }

    public static JSONArray getGroups(){
        return repository.readFileToJson(MyChatAppp.currentDev+"/groups.txt");
    }
}
