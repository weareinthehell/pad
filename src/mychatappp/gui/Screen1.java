package mychatappp.gui;

import mychatappp.MyChatAppp;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

public class Screen1 extends JFrame {
    private Container container;
    private Thread listen;
    private Thread dialogThread;
    private Socket socket;
    private JPanel rightPanel;
    private JTextField message;
    private JScrollPane dialog;
    private JPanel dialogPanel;
    private int global_width = 750;
    private int global_height = 900;

    public Screen1(){
        super("PAD LAB 1");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(new Dimension(global_width, global_height));
        setResizable(false);
        container = getContentPane();
        setUI();
        listen();
        setVisible(true);
    }

    private void setUI(){
        rightPanel();
        container.setLayout(new BorderLayout());
        container.add(topPanel(),BorderLayout.NORTH);
        container.add(leftPanel(),BorderLayout.WEST);
        container.add(rightPanel,BorderLayout.EAST);
    }

    private JPanel topPanel(){
        JPanel col = new JPanel();
        col.setLayout(new BoxLayout(col, BoxLayout.LINE_AXIS));
        col.setPreferredSize(new Dimension(global_width,50));

        JLabel label = new JLabel();
        label.setText("Hello "+ MyChatAppp.getName());
        label.setPreferredSize(new Dimension(global_width -150,50));
        label.setMaximumSize(new Dimension(global_width -150,50));
        label.setBackground(Color.gray);
        col.add(label);

        label = new JLabel();
        label.setText(MyChatAppp.getIp()+":"+MyChatAppp.getPort());
        label.setPreferredSize(new Dimension(150,50));
        label.setMaximumSize(new Dimension(150,50));
        col.add(label);

        return col;
    }

    private JPanel groupsPanel(int width){
        JPanel groupsPanel = new JPanel();
        groupsPanel.setLayout(new BoxLayout(groupsPanel, BoxLayout.Y_AXIS));
        groupsPanel.setPreferredSize(new Dimension(width,250));
        groupsPanel.setMaximumSize(new Dimension(width,250));
        groupsPanel.setBackground(Color.lightGray);

        for (int i=0;i<MyChatAppp.groups.length();i++){
            JPanel panel = new JPanel();
            panel.setBackground(Color.lightGray);
//            panel.setBackground(Color.GREEN);
            panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
            panel.setPreferredSize(new Dimension(width,50));
            panel.setMaximumSize(new Dimension(width,50));

            JLabel label1 = new JLabel();
            label1.setMaximumSize(new Dimension(width-100,50));
            label1.setPreferredSize(new Dimension(width-100,50));
            label1.setText(MyChatAppp.groups.getJSONObject(i).getString("name"));
            panel.add(label1,BorderLayout.WEST);
            boolean atGroup = MyChatAppp.groups.getJSONObject(i).getInt("here")==1;

            JButton button = new JButton();
            if (atGroup){
                button.setText("U");
                int finalI = i;
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        unsubscribeToGroup(MyChatAppp.groups.getJSONObject(finalI).getInt("port"));
                    }
                });
            }
            else {
                button.setText("S");
                int finalI = i;
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        subscribeToGroup(MyChatAppp.groups.getJSONObject(finalI).getInt("port"));
                    }
                });
            }
            panel.add(button,BorderLayout.WEST);
            if (atGroup) {
                button = new JButton();
                button.setText("O");
                int finalI1 = i;
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        try {
                            openDialog(MyChatAppp.groups.getJSONObject(finalI1).getInt("port"));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                panel.add(button, BorderLayout.WEST);
            }
            groupsPanel.add(panel);
        }
        return groupsPanel;
    }

    private JPanel usersPanel(int width){
        JPanel groupsPanel = new JPanel();
        groupsPanel.setLayout(new BoxLayout(groupsPanel, BoxLayout.Y_AXIS));
        groupsPanel.setPreferredSize(new Dimension(width,50));
        groupsPanel.setMaximumSize(new Dimension(width,50));
        groupsPanel.setBackground(Color.lightGray);

        for (int i=0;i<MyChatAppp.users.length();i++){
            JPanel panel = new JPanel();
            panel.setBackground(Color.lightGray);
//            panel.setBackground(Color.CYAN);
            panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
            panel.setPreferredSize(new Dimension(width,50));
            panel.setMaximumSize(new Dimension(width,50));

            JLabel label1 = new JLabel();
            label1.setMaximumSize(new Dimension(width,50));
            label1.setPreferredSize(new Dimension(width,50));
            label1.setText(MyChatAppp.users.getString(i));
            panel.add(label1,BorderLayout.WEST);
            groupsPanel.add(panel);
        }

        return groupsPanel;
    }

    private void rightPanel(){
        int width = 200;
        rightPanel = new JPanel();
        rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.Y_AXIS));
        rightPanel.setPreferredSize(new Dimension(width, global_height -50));
        rightPanel.setBackground(Color.lightGray);

        JLabel label = new JLabel();
        label.setText("Groups");
        label.setPreferredSize(new Dimension(width,50));
        label.setMaximumSize(new Dimension(width,50));
        rightPanel.add(label);

        rightPanel.add(groupsPanel(width));

        label = new JLabel();
        label.setText("Online users");
        label.setPreferredSize(new Dimension(width,50));
        label.setMaximumSize(new Dimension(width,50));
        rightPanel.add(label);
        rightPanel.add(usersPanel(width));
    }

    private void openDialog(int port) throws IOException {
        if (dialogThread != null) {
            dialogThread.interrupt();
        }

        socket = new Socket(MyChatAppp.host,port);

        JPanel col = new JPanel();
        col.setPreferredSize(new Dimension(global_width -200, global_height -50));
        col.setBackground(Color.white);
        col.setLayout(new BorderLayout());

        dialogPanel = new JPanel();
        dialogPanel.setBackground(Color.white);
        dialogPanel.setLayout(new BoxLayout(dialogPanel,BoxLayout.Y_AXIS));
        dialogPanel.setMinimumSize(new Dimension(global_width -200,750));

        dialog = new JScrollPane(dialogPanel);
        dialog.setBorder(new EmptyBorder(0,0,0,0));
        dialog.setViewportView(dialogPanel); // ********** changed *******

        dialog.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        dialog.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
//        dialog.setMaximumSize(new Dimension(width,750));
        dialog.setPreferredSize(new Dimension(global_width -200, global_height -150));

        JPanel leftFrame = new JPanel();
        leftFrame.setPreferredSize(new Dimension(global_width -200,75));
        leftFrame.setLayout(new BoxLayout(leftFrame,BoxLayout.Y_AXIS));

        message = new JTextField();
        message.setPreferredSize(new Dimension(global_width,50));
        message.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {
            }

            @Override
            public void keyPressed(KeyEvent keyEvent) {
                if (keyEvent.getKeyCode()==KeyEvent.VK_ENTER) {
                    try {
                        sendDialog(message.getText());
                        message.setText("");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        });
        leftFrame.add(message);

        JButton sendButton = new JButton();
        sendButton.setPreferredSize(new Dimension(global_width,25));
        sendButton.setMinimumSize(new Dimension(global_width,25));
        sendButton.setMaximumSize(new Dimension(global_width,25));
        sendButton.setText("Send");

        sendButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (message.getText().length()>0){
                    try {
                        sendDialog(message.getText());
                        message.setText("");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        leftFrame.add(sendButton);

        col.add(dialog,BorderLayout.NORTH);
        col.add(leftFrame,BorderLayout.SOUTH);
        container.remove(1);
        container.add(col,BorderLayout.WEST);
        container.setComponentZOrder(col,1);
        container.invalidate();
        container.validate();

        listenDialog();
    }


    private void listenDialog(){
        dialogThread = new Thread(new Runnable() {
            @Override
            public void run() {
                    while(dialogThread.isAlive()){
                        try {
                            do {
                                BufferedReader inFromClient = null;
                                inFromClient = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                                String message = inFromClient.readLine();
                                System.out.println(message);
                                JSONObject obj = new JSONObject(message);
                                if (obj.has("action")&&obj.getString("action").equals("store")){
                                    restore(obj.getJSONArray("messages"));
                                }
                                else appendToDialog(obj);


                            } while (socket.getInputStream().available() > 0);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
            }
        });
        dialogThread.start();
    }

    private void restore(JSONArray messages){
        for (int i=0;i<messages.length();i++)
            appendToDialog(messages.getJSONObject(i));
    }

    private void sendDialog(String message) throws IOException {
        GregorianCalendar c = new GregorianCalendar();
        SimpleDateFormat formatedDate = new SimpleDateFormat("dd.MM.YYYY k:m:s");

        JSONObject ms = new JSONObject();
        ms.put("port",MyChatAppp.getPort());
        ms.put("name",MyChatAppp.getName());
        ms.put("message",message);
        ms.put("date",formatedDate.format(c.getTime()));

        DataOutputStream stream = new DataOutputStream(socket.getOutputStream());
        stream.writeBytes(ms.toString()+"\n");
    }

    private void subscribeToGroup(int port){
        try {
            DataOutputStream stream = new DataOutputStream(MyChatAppp.getClientSocket().getOutputStream());
            JSONObject action = new JSONObject();
            action.put("action","subscribe");
            action.put("port",port);
            stream.writeBytes(action.toString()+"\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void appendToDialog(JSONObject message) {
        JPanel messagePane = new JPanel();
        messagePane.setPreferredSize(new Dimension(global_width -230, 50));
        messagePane.setMaximumSize(new Dimension(global_width -230, 50));
        messagePane.setLayout(new BorderLayout());
        messagePane.setBackground(Color.white);
        messagePane.setBorder(new BevelBorder(BevelBorder.LOWERED));

        JLabel name = new JLabel();
        if (message.getString("name").equals(MyChatAppp.getName()) && message.getInt("port") == MyChatAppp.getPort())
            name.setText("ME");
        else
            name.setText(message.getString("name"));
        name.setFont(new Font("Arial", Font.BOLD, 15));

        JLabel text = new JLabel();
        text.setPreferredSize(new Dimension(global_width -230, 20));
        text.setText(message.getString("message"));
        text.setFont(new Font("Colibri", Font.PLAIN, 13));

        JLabel date = new JLabel();
        date.setText(message.getString("date"));
        date.setFont(new Font("Colibri", Font.PLAIN, 13));

        if (message.getString("name").equals(MyChatAppp.getName()) && message.getInt("port") == MyChatAppp.getPort()) {
            text.setHorizontalAlignment(SwingConstants.RIGHT);
            messagePane.add(name, BorderLayout.EAST);
            messagePane.add(date, BorderLayout.WEST);
            messagePane.add(text,BorderLayout.SOUTH);
        }
        else{
            messagePane.add(name, BorderLayout.WEST);
            messagePane.add(date, BorderLayout.EAST);
            messagePane.add(text,BorderLayout.SOUTH);
        }

        dialogPanel.add(messagePane);
        dialogPanel.invalidate();
        dialogPanel.validate();
    }

    private void unsubscribeToGroup(int port){
        try {
            DataOutputStream stream = new DataOutputStream(MyChatAppp.getClientSocket().getOutputStream());
            JSONObject action = new JSONObject();
            action.put("action","unsubscribe");
            action.put("port",port);
            stream.writeBytes(action.toString()+"\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void listen(){
        listen =  new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while(true){
                        do {
                            BufferedReader inFromClient = new BufferedReader(new InputStreamReader(MyChatAppp.getClientSocket().getInputStream()));
                            String message = inFromClient.readLine();

                            if (!message.equals("Test")) {
                                JSONObject obj = null;
                                try {
                                    obj = new JSONObject(message);
                                }
                                catch (Exception e){
                                    listen.interrupt();
                                    TimeUnit.SECONDS.sleep(3);
                                    listen();
                                }
                                if (obj.has("groups")) {
                                    MyChatAppp.groups = obj.getJSONArray("groups");
                                }
                                MyChatAppp.users = obj.getJSONArray("users");
                                rightPanel();
                                container.remove(container.getComponentCount() - 1);
                                container.add(rightPanel,BorderLayout.EAST);
                                container.setComponentZOrder(rightPanel,2);
                                container.invalidate();
                                container.validate();
                            }

                        } while (MyChatAppp.getClientSocket().getInputStream().available() > 0);
                    }
                } catch (Throwable e) {
                    listen.interrupt();
                    try {
                        TimeUnit.SECONDS.sleep(3);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                    listen();
                    e.printStackTrace();
                }
            }
        });
        listen.start();
    }

    private JPanel leftPanel(){
        int width = global_width -200;
        JPanel col = new JPanel();
        col.setLayout(new BoxLayout(col, BoxLayout.LINE_AXIS));
        col.setPreferredSize(new Dimension(width, global_height -50));
        col.setBackground(Color.white);

        JLabel label = new JLabel();
        label.setText("Chat");
        label.setPreferredSize(new Dimension(width,50));
        label.setMaximumSize(new Dimension(width,50));
        col.add(label);

        return col;
    }

}
