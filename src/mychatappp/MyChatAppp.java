package mychatappp;

import mychatappp.data.Repository;
import mychatappp.gui.MainScreen;
import mychatappp.gui.Screen;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketException;
import java.nio.channels.SocketChannel;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.GregorianCalendar;

public class MyChatAppp {
    private static String ip;
    private static String name;
    private static int port = 8080;
    private static Socket clientSocket;
    public static String currentDev;
    public static Repository repository;
    public static JSONArray groups;
    public static JSONArray users = new JSONArray();
    public static String host = "127.0.0.1";

    static {
        currentDev = System.getProperty("user.dir");
        repository = new Repository();
    }

    public static void main(String[] args) throws IOException {
        try {
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements()) {
                NetworkInterface iface = interfaces.nextElement();
                // filters out 127.0.0.1 and inactive interfaces
                if (iface.isLoopback() || !iface.isUp())
                    continue;

                Enumeration<InetAddress> addresses = iface.getInetAddresses();
                while(addresses.hasMoreElements()) {
                    InetAddress addr = addresses.nextElement();
                    if (addr.getHostAddress().length() < 25)
                    setIp(addr.getHostAddress());
                    if (getName() == null || getName().equals("Guest"))
                        setName(InetAddress.getLocalHost().getHostName());
                }
            }
        } catch (SocketException e) {
            throw new RuntimeException(e);
        }

        setClientSocket(new Socket(host, 8999));
        DataOutputStream outToServer = new DataOutputStream(getClientSocket().getOutputStream());
        outToServer.writeBytes(getIp()+" "+getName()+"\n");
        BufferedReader inFromClient =
                new BufferedReader(new InputStreamReader(getClientSocket().getInputStream()));

        String message = inFromClient.readLine();

        JSONObject obj = new JSONObject(message);
        groups = obj.getJSONArray("groups");
        users = obj.getJSONArray("users");
        setPort(obj.getInt("port"));

        Screen screen = new Screen();
        screen.show();

    }


    public static String getIp(){
        return MyChatAppp.ip;
    }

    public static String getName(){
        return MyChatAppp.name;
    }

    public static int getPort(){
        return MyChatAppp.port;
    }

    public static void setIp(String ip){
        MyChatAppp.ip = ip;
    }

    public static void setName(String name){
        GregorianCalendar c = new GregorianCalendar();
        MyChatAppp.name = name;
    }

    public static void setPort(int port){
        MyChatAppp.port = port;
    }

    public static Socket getClientSocket() {
        return MyChatAppp.clientSocket;
    }

    public static void setClientSocket(Socket socket) {
        MyChatAppp.clientSocket = socket;
    }
}
